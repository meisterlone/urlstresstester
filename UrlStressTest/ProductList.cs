﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UrlStressTest
{

    public class ProductList
    {
        public int pages { get; set; }
        public int current_page { get; set; }
        public int page_items_total { get; set; }
        public int search_items_total { get; set; }
        public List<string> size_list { get; set; }
        public List<string> color_list { get; set; }
        public List<string> tag_list { get; set; }
        public Dictionary<string, List<string>> filter_tags { get; set; }
        public Dictionary<int, string> vendor_list { get; set; }
        public List<ProductListItem> result_items { get; set; }
    }

    //public class VendorListItem
    //{
    //    public int id { get; set; }
    //    public string name { get; set; }
    //}

    public class ProductListItem
    {
        public int id { get; set; }
        public string title { get; set; }
        public string vendor { get; set; }
        public long[] favourited { get; set; }
        public long[] purchased { get; set; }
        public long[] rented { get; set; }
        public double[] pds { get; set; }
        public int pos { get; set; }
        public DateTime created { get; set; }
        public long shopify_id { get; set; }
        public List<string> images { get; set; }
        public List<string> colors { get; set; }
        public Dictionary<string, int?> sizes { get; set; }
        public List<string> general_sizes { get; set; }

        [JsonIgnore]
        public List<string> tags { get; set; }



    }

}
