﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UrlStressTest
{
    class Program
    {

        static long successResultCount = 0;
        static long failResultCount = 0;
        static double totalMilliseconds = 0;

  
        static void Main(string[] args)
        {
            Task.Run(threadOutput);
            new Thread(threadTester).Start();
            new Thread(threadTester).Start();
            new Thread(threadTester).Start();
            new Thread(threadTester).Start();
            new Thread(threadTester).Start();
            new Thread(threadTester).Start();
            new Thread(threadTester).Start();



        }

        public static async void threadOutput()
        {
            Stopwatch sw = Stopwatch.StartNew();

            while (true)
            {
                await Task.Delay(100);

                if (sw.Elapsed.Seconds == 0)
                    continue;
                if (successResultCount == 0)
                    continue;

                double sPerSecond = successResultCount / (double)sw.Elapsed.TotalSeconds;
                double fPerSecond = failResultCount / (double)sw.Elapsed.TotalSeconds;
                double averageResponseTime = (totalMilliseconds / successResultCount);

                Console.WriteLine("success: " + sPerSecond.ToString("0.00") + " requests per second, avg response time: "+ averageResponseTime.ToString("0") + "ms , fail #: " + Math.Round(fPerSecond, 2));


            }
        }


        public static void threadTester()
        {

            WebClient wc = new WebClient();

            Stopwatch sw = new Stopwatch();
            while (true)
            {
                try
                {
                    sw = Stopwatch.StartNew();
                    string result = wc.DownloadString("http://localhost:52246/api/v1/Product/Collections/Search?page=1&items_per_page=30&category=all&filter_tags_all=tops&in_stock=1&filter_vendor_ids=");
                    sw.Stop();
                    //var gg = JsonConvert.DeserializeObject<ProductList>(result);
                    if (result.StartsWith("{\"pages\":"))
                    {
                        Interlocked.Increment(ref successResultCount);
                        totalMilliseconds += sw.Elapsed.TotalMilliseconds;
                    }
                    else
                        Interlocked.Increment(ref failResultCount);
                }
                catch (Exception ex)
                {
                    Interlocked.Increment(ref failResultCount);
                }

            }
        }
    }
}
